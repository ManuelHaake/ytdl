# ytdl

**NOTICE! Whether you are allowed to use youtube-dl at all depends on the legal framework in your home country or your location!**

## What does it do?
This is a script to download Youtube- or Twitch-Videos in best available quality with the given Link $1 at the desired Speed $2 in MB/s.
Its not much more than an alias, but it increases effectiveness.

## Usage:
Download a video with max available speed:

 `bash ytdl https://www.link-to-video.com`

Download a video with speed limitation:

`bash ytdl https://www.link-to-video.com 2` to download with 2MB/s

`bash ytdl https://www.link-to-video.com 5` to download with 5MB/s

If you copy the script to `/usr/local/bin/` via `sudo cp ytdl /usr/local/bin/` , you can run it in the command line without the bash prefix from everywhere.
Make sure to make the file executeable with `sudo chmod +x /usr/local/bin/ytdl`
